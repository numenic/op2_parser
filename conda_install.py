#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""conda install from requirements.txt
"""
import logging
import os
import re
import shlex
import subprocess

PKGREGEX = re.compile(r"^\w+")
CONDABIN = "/opt/anaconda3/bin/conda"


def install_from(fname):
    # ========================================================================
    # first install with anaconda
    # ========================================================================
    status = []
    failed = {}
    logging.info(f"install from {fname}")
    with open(fname, "r") as fh:
        for line in fh:
            line = line.split("#")[0].strip()
            if not line:
                continue
            cmd = f" {CONDABIN} install --yes {line}"
            args = cmd.format()
            # get package name
            pkg = PKGREGEX.findall(line)[0]
            args = shlex.split(args)
            try:
                subprocess.run(args, check=True)
                status.append((line, "conda"))
            except Exception as exc:
                logging.warning(f'cannot conda install "{pkg}"')
                failed[pkg] = line
    # ========================================================================
    # failed packages installation using PIP
    # ========================================================================
    # find current environment's pip
    pip = os.path.join(os.environ["CONDA_PREFIX"], "bin", "pip")
    for pkg in failed.copy():
        specs = failed.pop(pkg)
        cmd = f"{pip} install -U {pkg}"
        print(cmd)
        args = shlex.split(cmd)
        try:
            subprocess.run(args, check=True)
            status.append((specs, "pip"))
        except Exception as exc:
            logging.error(f'cannot pip install "{pkg}"')
            failed[pkg] = specs
    print(80 * "=")
    print("successfully installed:")
    for specs, src in status:
        print(f"  * {specs} ({src})")
    print(80 * "=")
    if failed:
        print("Got some errors:")
        for pkg, specs in failed.items():
            logging.error(f"cannot install {specs}")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    args = parser.parse_args()
    install_from(fname=args.file)
