==========
op2_parser
==========


.. image:: https://img.shields.io/pypi/v/op2_parser.svg
        :target: https://pypi.python.org/pypi/op2_parser



.. image:: https://readthedocs.org/projects/op2-parser/badge/?version=latest
        :target: https://op2-parser.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


Simple wrapper to pyNastran OP2 results file


* Free software: MIT license
* Documentation: https://op2-parser.readthedocs.io.


Features
--------

* TODO

