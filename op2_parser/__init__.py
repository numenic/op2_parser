# -*- coding: utf-8 -*-

"""Top-level package for op2_parser."""

__author__ = """Nicolas Cordier"""
__email__ = "nicolas.cordier@numeric-gmbh.ch"
__version__ = "0.1.0"

from op2_parser.op2_parser import Parser
